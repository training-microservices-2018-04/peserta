package com.artivisi.training.microservice201804.peserta.service;

import com.artivisi.training.microservice201804.peserta.dto.EmailRequest;
import com.artivisi.training.microservice201804.peserta.entity.Peserta;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSenderService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.topic.email.request}")
    private String topicEmailRequest;

    @Autowired private ObjectMapper objectMapper;

    public void kirimEmailSelamatBergabung(Peserta peserta) {
        EmailRequest email = EmailRequest.builder()
                .from("admin@training.com")
                .to(peserta.getEmail())
                .subject("Selamat Bergabung")
                .content("Terima kasih telah bergabung di lokasi pelatihan")
                .build();

        try {
            String json = objectMapper.writeValueAsString(email);
            LOGGER.debug("Email Request : {}", json);
            kafkaTemplate.send(topicEmailRequest, json);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
}
