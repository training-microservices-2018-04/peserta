package com.artivisi.training.microservice201804.peserta.controller;

import com.artivisi.training.microservice201804.peserta.dao.PesertaDao;
import com.artivisi.training.microservice201804.peserta.entity.Peserta;
import com.artivisi.training.microservice201804.peserta.service.KafkaSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController @RequestMapping("/api/peserta")
public class PesertaApiController {

    @Autowired
    private PesertaDao pesertaDao;

    @Autowired private KafkaSenderService kafkaSenderService;

    @GetMapping("/")
    public Iterable<Peserta> semuaPeserta() {
        return pesertaDao.findAll();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void simpan(@RequestBody @Valid Peserta peserta) {
        pesertaDao.save(peserta);
        kafkaSenderService.kirimEmailSelamatBergabung(peserta);
    }
}
