package com.artivisi.training.microservice201804.peserta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PesertaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PesertaApplication.class, args);
	}
}
