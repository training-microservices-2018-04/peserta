package com.artivisi.training.microservice201804.peserta.dao;

import com.artivisi.training.microservice201804.peserta.entity.Peserta;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {
}
