package com.artivisi.training.microservice201804.peserta.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class HostController {

    @GetMapping("/api/host")
    public Map<String, String> host(HttpServletRequest request) {
        Map<String, String> hasil = new HashMap<>();
        hasil.put("IP", request.getLocalAddr());
        hasil.put("Port", String.valueOf(request.getLocalPort()));
        hasil.put("Host", request.getLocalName());
        return hasil;
    }
}
