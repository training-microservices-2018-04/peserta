package com.artivisi.training.microservice201804.peserta.dto;

import lombok.*;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class EmailRequest {

    @NonNull
    private String to;

    @NonNull
    private String from;

    @NonNull
    private String subject;

    @NonNull
    private String content;
}
