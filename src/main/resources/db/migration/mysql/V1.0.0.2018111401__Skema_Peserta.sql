CREATE TABLE peserta (
  id varchar(36) NOT NULL,
  email varchar(255) NOT NULL,
  no_hp varchar(255) NOT NULL,
  nomor varchar(255) NOT NULL,
  nama_belakang varchar(255) NOT NULL,
  nama_depan varchar(255) NOT NULL,
  foto varchar(255) DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=latin1;